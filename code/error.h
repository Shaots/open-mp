//
// Created by Lenovo on 20.10.2023.
//

#ifndef PARALAB_ERROR_H
#define PARALAB_ERROR_H

#include <stdio.h>
#include <stdlib.h>

typedef enum {
    ///Ok
    ERROR_OK,

    ///Invalid argument
    ERROR_INVALID_ARGUMENT,

    /// Null pointer
    ERROR_NULL,

    /// Dimension dos not match
    ERROR_DIMENSION,

    ///Invalid program argument
    ERROR_INVALID_PROGRAM_ARGUMENT,

    ///File cannot be opened
    ERROR_FILE_CANNOT_BE_OPENED,

    ///Memory is not enough
    ERROR_MEMORY,

    ///Reading file is failed
    ERROR_READFILE
} myError;


FILE* getLogger();


void setLogger(FILE* out);


char const* getErrorString(myError myError);


void reportError(myError myError, const char* _file_, const char* _function_, int line);


#endif //PARALAB_ERROR_H
