//
// Created by Lenovo on 20.10.2023.
//

#include "error.h"

static FILE* logger = NULL;


FILE* getLogger(){
    return logger;
}


void setLogger(FILE* out){
    logger = out;
}


char const* getErrorString(myError myError){
    switch (myError){
        case(ERROR_OK):
            return "Ok, no error\0";
            break;
        case(ERROR_INVALID_ARGUMENT):
            return "Invalid argument\0";
            break;
        case (ERROR_DIMENSION):
            return "Dimension does not match\0";
            break;
        case (ERROR_NULL):
            return "NULL pointer\0";
            break;
        case(ERROR_INVALID_PROGRAM_ARGUMENT):
            return "Invalid program's argument\0";
            break;
        case(ERROR_FILE_CANNOT_BE_OPENED):
            return "File cannot be opened\0";
            break;
        case(ERROR_MEMORY):
            return "Memory is not enough\0";
            break;
        case(ERROR_READFILE):
            return "File cannot be normally read\0";
            break;
        default:
            return "Unknown Error\0";
    }
}


void reportError(myError myError, const char* _file_, const char* _function_, int line){
    if (logger){
        fprintf(logger, "File: %s Func: %s Line: %d ERROR: %s\n", _file_, _function_, line, getErrorString(myError));
    }
}
