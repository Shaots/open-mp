//
// Created by Lenovo on 30.11.2023.
//

#include "omp_vector.h"

vector* creatVector(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (allocator == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* vec = (vector*) allocator(1, sizeof(vector), err);
    if (vec == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vec->data = (double*) allocator(n, sizeof(double), err);
    if (vec->data == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        free(vec);
        return NULL;
    }
    vec->n = n;
    return vec;
}


void copyData(vector* from, vector* to, myError* err) {
    if (from->n != to->n) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    memcpy(to->data, from->data, from->n * sizeof(double));
}


double omp_dotProduct(vector* v1, vector* v2, myError* err) {
    if (v1 == NULL || v2 == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t dim = v1->n;
    if (v2->n != dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    double sum = 0;
    size_t i;
#pragma omp parallel for shared(v1, v2, dim) private(i) reduction(+: sum) default(none)
    for (i = 0; i < dim; ++i) {
        sum += v1->data[i] * v2->data[i];
    }
    return sum;
}

double omp_norm2(vector* v, myError* err) {
    if (v == NULL) {
        return NAN;
    }
    return sqrt(omp_dotProduct(v, v, err));
}


double omp_normInf(vector* v, myError* err) {
    if (v == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t dim = v->n;
    double sum = fabs(v->data[0]);
    size_t i;
#pragma omp parallel for shared(v, dim) private(i) reduction(max: sum) default(none)
    for (i = 0; i < dim; ++i) {
        sum = fabs(v->data[i]);
    }

    return sum;
}


// z = ax + y
vector* omp_saxpy(double a, vector* v1, vector* v2, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (v1 == NULL || v2 == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = v1->n;
    if (v2->n != dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* z = creatVector(dim, allocator, err);
    if (z == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    size_t i;
#pragma omp parallel for shared(a, v1, v2, dim, z) private(i) default(none)
    for (i = 0; i < dim; ++i) {
        z->data[i] = a * v1->data[i] + v2->data[i];
    }

    return z;
}


void deleteVector(vector* vec, myError* err) {
    if (vec == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    free(vec->data);
    free(vec);
}


void paintVector(vector* vec, myError* err) {
    if (vec == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    size_t dim = vec->n;
    printf("(");
    for (size_t i = 0; i < dim - 1; ++i) {
        printf("%lf    ", vec->data[i]);
    }
    printf("%lf)\n", vec->data[dim - 1]);
}