//
// Created by Lenovo on 27.11.2023.
//

#ifndef PARAMAIN_HEATEQUATION_H
#define PARAMAIN_HEATEQUATION_H

#include <omp.h>
#include <math.h>
#include "omp_vector.h"
#include "omp_sparse.h"
#include "myMath.h"
#include "square.h"
#include "../error.h"
#include "../memory.h"

#define PI 3.1415926535897932384626433832795028841971

typedef struct indexAngle {
    size_t leftDown;
    size_t leftUp;
    size_t rightDown;
    size_t rightUp;
} indexAngle;

typedef struct ellipticProb {
    square sq;
    size_t n;
    double h;

    indexAngle iA;
    size_t* indexLeft;
    size_t* indexDown;
    size_t* indexUp;
    size_t* indexRight;
} ellipticProb;

// du/dt - a^2 div (grad u) = f
typedef struct {
    ellipticProb* elliptic;
    double tau;
    double endTime;
    double diffusivity; //a
} heatTransfer;


double omp_ut(double x, double y, double t);


double omp_f(double x, double y, double t, double diffusivity);


double omp_u(double x, double y);


ellipticProb* omp_creatElliptic(square sq, size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err);


heatTransfer*
createHeat(square sq, size_t n, double tau, double diffusivity,
           void* const (* allocator)(size_t, size_t, myError*), myError* err);


sparseMatrix* omp_calStiffness(heatTransfer* heat, void* const (* allocator)(size_t, size_t, myError*), myError* err);


vector* omp_fiveDiagMatrixMultiplyVec(heatTransfer* heat, sparseMatrix* A, vector* x,
                                      void* const (* allocator)(size_t, size_t, myError*), myError* err);


vector*
omp_calForce(heatTransfer* heat, vector* previousLayer, double t, void* const (* allocator)(size_t, size_t, myError*),
             myError* err);


vector*
omp_conjGrad(heatTransfer* heat, sparseMatrix* A, vector* b, void* const (* allocator)(size_t, size_t, myError*),
             myError* err);


vector* omp_findEndTime(heatTransfer* heat, void* const (* allocator)(size_t, size_t, myError*), myError* err);


double omp_calErrInf(heatTransfer* heat, vector* v, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void deleteEllipticProb(ellipticProb* ell, myError* err);


void deleteHeat(heatTransfer* heat, myError* err);


#endif //PARAMAIN_HEATEQUATION_H
