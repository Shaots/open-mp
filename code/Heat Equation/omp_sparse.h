#ifndef OPEN_MP_OMP_SPARSE_H
#define OPEN_MP_OMP_SPARSE_H

#include <stdio.h>
#include <stdlib.h>
#include "omp_vector.h"
#include "../error.h"

typedef struct sparseMatrix {
    vector* vec;
    size_t* rowIndex;
    size_t* colIndex;
} sparseMatrix;


sparseMatrix* createSparseMatrix(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void sparseSetCoord(sparseMatrix* sp, size_t index, size_t row, size_t col, double value, myError* err);


void deleteSparseMatrix(sparseMatrix* sp, myError* err);


void paintSparse(sparseMatrix* sp, myError* err);


#endif //OPEN_MP_OMP_SPARSE_H
