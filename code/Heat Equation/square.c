//
// Created by Lenovo on 21.10.2023.
//

#include "square.h"

square creatSquare(point leftBottom, double sideLength) {
    square sq;
    if (sideLength <= 0) {
        sq.leftBottom.x = leftBottom.x + sideLength;
        sq.leftBottom.y = leftBottom.y + sideLength;
        sq.sideLength = -sideLength;
    } else {
        sq.leftBottom.x = leftBottom.x;
        sq.leftBottom.y = leftBottom.y;
        sq.sideLength = sideLength;
    }
    return sq;
}


void paintSquare(square sq) {
    printf("Left bottom point: (%lf, %lf),    Length of side: %lf\n", sq.leftBottom.x, sq.leftBottom.y, sq.sideLength);
}