//
// Created by Lenovo on 20.10.2023.
//
#include "omp_sparse.h"

sparseMatrix* createSparseMatrix(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (allocator == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    sparseMatrix* sp = (sparseMatrix*) allocator(1, sizeof(sparseMatrix), err);
    if (sp == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    sp->vec = creatVector(n, allocator, err);
    sp->colIndex = (size_t*) allocator(n, sizeof(size_t), err);
    sp->rowIndex = (size_t*) allocator(n, sizeof(size_t), err);
    if (sp->vec == NULL || sp->colIndex == NULL || sp->rowIndex == NULL) {
        free(sp->colIndex);
        free(sp->rowIndex);
        deleteVector(sp->vec, err);
        free(sp);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    return sp;
}


void sparseSetCoord(sparseMatrix* sp, size_t index, size_t row, size_t col, double value, myError* err) {
    if (sp == NULL || index >= sp->vec->n) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    sp->vec->data[index] = value;
    sp->rowIndex[index] = row;
    sp->colIndex[index] = col;
}


void deleteSparseMatrix(sparseMatrix* sp, myError* err) {
    if (sp == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    free(sp->colIndex);
    free(sp->rowIndex);
    deleteVector(sp->vec, err);
    free(sp);
}


void paintSparse(sparseMatrix* sp, myError* err){
    if(sp == NULL){
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    for(size_t i = 0; i < sp->vec->n; ++i){
        printf("(%zd, %zd)    %lf\n", sp->rowIndex[i], sp->colIndex[i], sp->vec->data[i]);
    }
}