//
// Created by Lenovo on 25.10.2023.
//

#include "myMath.h"

vector*
linspace(double start, double end, size_t num, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    double h = (end - start) / (double) (num - 1);
    vector* vec = creatVector(num, allocator, err);
    if (vec == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    for (size_t i = 0; i < num; ++i) {
        vec->data[i] = start + (double) i * h;
    }
    return vec;
}
