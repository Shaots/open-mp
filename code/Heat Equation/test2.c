//
// Created by Lenovo on 01.12.2023.
//

#include "test2.h"

void testVector(myError* err) {
    size_t len1 = 5;
    vector* vec1 = creatVector(len1, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    for (size_t i = 0; i < len1; ++i) {
        vec1->data[i] = (double) (i * i);
    }
    double vec1Vec1 = omp_dotProduct(vec1, vec1, err);
    printf("(vec1, vec1) = %lf\n", vec1Vec1);
}


void testHeat(myError* err) {
    point p1;
    p1.x = 0;
    p1.y = 0;
    double sideLen = 1;
    square sq = creatSquare(p1, sideLen);
    double diffusivity = 3;
    omp_set_num_threads(8);
    FILE* file = fopen("result time.txt", "ab");
    size_t n[100];
    double start_time, end_time;
    size_t times = 1;
    for (int i = 0; i < 1; ++i) {
        n[i] = 257;
        start_time = omp_get_wtime();
        double tau = 1.0 / (double) ((n[i] - 1) * (n[i] - 1));
        for (int j = 0; j < times; ++j) {
            heatTransfer* heat = createHeat(sq, n[i], tau, diffusivity,
                                            (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
            vector* res = omp_findEndTime(heat, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
            //double diff = omp_calErrInf(heat, res, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
            //printf("diff = %.10lf,    T = %lf,    tau = %lf\n", diff, heat->endTime, heat->tau);
            deleteVector(res, err);
            deleteHeat(heat, err);
        }
        end_time = omp_get_wtime();
        fprintf(file, "%lf\n", (end_time - start_time) / (float) times);
    }
    fclose(file);
}