//
// Created by Lenovo on 30.11.2023.
//

#ifndef PARAMAIN_OMP_VECTOR_H
#define PARAMAIN_OMP_VECTOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <math.h>
#include "../error.h"

typedef struct vector {
    size_t n;
    double* data;
} vector;

vector* creatVector(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void copyData(vector* from, vector* to, myError* err);


double omp_dotProduct(vector* v1, vector* v2, myError* err);


double omp_norm2(vector* v, myError* err);


double omp_normInf(vector* v, myError* err);


vector* omp_saxpy(double a, vector* v1, vector* v2, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void deleteVector(vector* vec, myError* err);


void paintVector(vector* vec, myError* err);


#endif //PARAMAIN_OMP_VECTOR_H
