//
// Created by Lenovo on 27.11.2023.
//

#include "heatEquation.h"


double omp_ut(double x, double y, double t) {
    return cos(PI * x) * sin(PI * y) * (exp(-t) + 1);
}


double omp_f(double x, double y, double t, double diffusivity) {
    return cos(PI * x) * sin(PI * y) *
           (exp(-t) * (2 * diffusivity * diffusivity * PI * PI - 1) + 2 * diffusivity * diffusivity * PI * PI);
}


double omp_u(double x, double y) {
    return cos(PI * x) * sin(PI * y);
    // return x * x * y * y;
}

ellipticProb*
omp_creatElliptic(square sq, size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    size_t dim = n - 2;
    ellipticProb* ell;
    if (allocator == NULL || n < 5) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    ell = allocator(1, sizeof(ellipticProb), err);
    if (ell == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    //size_t numNodeAtBoundary = 4 * (dim - 2);
    ell->indexLeft = NULL;
    ell->indexDown = NULL;
    ell->indexUp = NULL;
    ell->indexRight = NULL;
    ell->iA.leftDown = 0;
    ell->iA.leftUp = dim - 1;
    ell->iA.rightDown = dim * (dim - 1);
    ell->iA.rightUp = dim * dim - 1;
    ell->sq = sq;
    ell->n = n;
    ell->h = ell->sq.sideLength / (double) (ell->n - 1);
    return ell;
}


heatTransfer*
createHeat(square sq, size_t n, double tau, double diffusivity,
           void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (allocator == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    if (tau <= 0 || diffusivity <= 0) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    ellipticProb* ell = omp_creatElliptic(sq, n, allocator, err);
    if (ell == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    heatTransfer* heat = (heatTransfer*) allocator(1, sizeof(heatTransfer), err);
    if (heat == NULL) {
        deleteEllipticProb(ell, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    heat->elliptic = ell;
    heat->tau = tau;
    heat->diffusivity = diffusivity;
    return heat;
}


sparseMatrix* omp_calStiffness(heatTransfer* heat, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (heat == NULL || heat->elliptic == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = heat->elliptic->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    sparseMatrix* sp = createSparseMatrix(numNoZero, allocator, err);
    if (sp == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    double diameter = heat->elliptic->h;
    double a = heat->diffusivity;

    size_t row;
    size_t col;
    size_t counterSp;
    size_t counter;

    // Left down
    counterSp = 0;
    counter = 0;
    sparseSetCoord(sp, counterSp++, counter, counter,
                   4 * a * a * heat->tau / (diameter * diameter) + 1, err);
    sparseSetCoord(sp, counterSp++, counter, counter + 1,
                   -1 * a * a * heat->tau / (diameter * diameter), err);
    sparseSetCoord(sp, counterSp++, counter, counter + dim,
                   -1 * a * a * heat->tau / (diameter * diameter), err);

    // Left up
    counterSp = 3 + (dim - 2) * 4;
    counter = dim - 1;
    sparseSetCoord(sp, counterSp++, counter, counter - 1,
                   -1 * a * a * heat->tau / (diameter * diameter), err);
    sparseSetCoord(sp, counterSp++, counter, counter,
                   4 * a * a * heat->tau / (diameter * diameter) + 1, err);
    sparseSetCoord(sp, counterSp++, counter, counter + dim,
                   -1 * a * a * heat->tau / (diameter * diameter), err);

    // Right down
    counter = (dim - 1) * dim;
    counterSp = 2 * 3 + 3 * (dim - 2) * 4 + (dim - 2) * (dim - 2) * 5;
    sparseSetCoord(sp, counterSp++, counter, counter - dim,
                   -1 * a * a * heat->tau / (diameter * diameter), err);
    sparseSetCoord(sp, counterSp++, counter, counter,
                   4 * a * a * heat->tau / (diameter * diameter) + 1, err);
    sparseSetCoord(sp, counterSp++, counter, counter + 1,
                   -1 * a * a * heat->tau / (diameter * diameter), err);

    // Right up
    counter = dim * dim - 1;
    counterSp = 3 * 3 + 4 * (dim - 2) * 4 + (dim - 2) * (dim - 2) * 5;
    sparseSetCoord(sp, counterSp++, counter, counter - dim,
                   -1 * a * a * heat->tau / (diameter * diameter), err);
    sparseSetCoord(sp, counterSp++, counter, counter - 1,
                   -1 * a * a * heat->tau / (diameter * diameter), err);
    sparseSetCoord(sp, counterSp++, counter, counter,
                   4 * a * a * heat->tau / (diameter * diameter) + 1, err);

    // Left
#pragma omp parallel for shared(heat, sp, dim, a, diameter, err) private(counterSp, counter, row) default(none)
    for (row = 1; row < dim - 1; ++row) {
        counter = row;
        counterSp = 3 + (row - 1) * 4;
        sparseSetCoord(sp, counterSp++, counter, counter - 1,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter,
                       4 * a * a * heat->tau / (diameter * diameter) + 1, err);
        sparseSetCoord(sp, counterSp++, counter, counter + 1,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter + dim,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
    }


    // Down
#pragma omp parallel for shared(heat, sp, dim, a, diameter, err) private(counterSp, counter, col) default(none)
    for (col = 1; col < dim - 1; ++col) {
        counter = col * dim;
        counterSp = 2 * 3 + (dim - 2) * 4 + (col - 1) * (2 * 4 + (dim - 2) * 5);
        sparseSetCoord(sp, counterSp++, counter, counter - dim,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter,
                       4 * a * a * heat->tau / (diameter * diameter) + 1, err);
        sparseSetCoord(sp, counterSp++, counter, counter + 1,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter + dim,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
    }

    // Up
#pragma omp parallel for shared(heat, sp, dim, a, diameter, err) private(counterSp, counter, col) default(none)
    for (col = 1; col < dim - 1; ++col) {
        counter = (col + 1) * dim - 1;
        counterSp = 2 * 3 + (dim - 2) * 4 + 4 + (dim - 2) * 5 + (col - 1) * (2 * 4 + (dim - 2) * 5);
        sparseSetCoord(sp, counterSp++, counter, counter - dim,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter - 1,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter,
                       4 * a * a * heat->tau / (diameter * diameter) + 1, err);
        sparseSetCoord(sp, counterSp++, counter, counter + dim,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
    }

    // Right
#pragma omp parallel for shared(heat, sp, dim, a, diameter, err) private(counterSp, counter, row) default(none)
    for (row = 1; row < dim - 1; ++row) {
        counter = (dim - 1) * dim + row;
        counterSp = 3 * 3 + 3 * (dim - 2) * 4 + (dim - 2) * (dim - 2) * 5 + (row - 1) * 4;
        sparseSetCoord(sp, counterSp++, counter, counter - dim,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter - 1,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
        sparseSetCoord(sp, counterSp++, counter, counter,
                       4 * a * a * heat->tau / (diameter * diameter) + 1, err);
        sparseSetCoord(sp, counterSp++, counter, counter + 1,
                       -1 * a * a * heat->tau / (diameter * diameter), err);
    }


#pragma omp parallel for shared(heat, sp, dim, a, diameter, err) private(counterSp, counter, row, col) collapse(2) default(none)
    for (col = 1; col < dim - 1; ++col) {
        for (row = 1; row < dim - 1; ++row) {
            counterSp = 2 * 3 + (dim - 2) * 4 + (2 * 4 + (dim - 2) * 5) * (col - 1) + 4 + (row - 1) * 5;
            counter = col * dim + row;
            sparseSetCoord(sp, counterSp++, counter, counter - dim,
                           -1 * a * a * heat->tau / (diameter * diameter), err);
            sparseSetCoord(sp, counterSp++, counter, counter - 1,
                           -1 * a * a * heat->tau / (diameter * diameter), err);
            sparseSetCoord(sp, counterSp++, counter, counter,
                           4 * a * a * heat->tau / (diameter * diameter) + 1, err);
            sparseSetCoord(sp, counterSp++, counter, counter + 1,
                           -1 * a * a * heat->tau / (diameter * diameter), err);
            sparseSetCoord(sp, counterSp++, counter, counter + dim,
                           -1 * a * a * heat->tau / (diameter * diameter), err);
            counter++;
        }
    }
    return sp;
}


vector*
omp_calForce(heatTransfer* heat, vector* previousLayer, double t, void* const (* allocator)(size_t, size_t, myError*),
             myError* err) {
    if (heat == NULL || heat->elliptic == NULL || previousLayer == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = heat->elliptic->n - 2;
    if (previousLayer->n != dim * dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* force = creatVector(dim * dim, allocator, err);
    if (force == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    double startX = heat->elliptic->sq.leftBottom.x;
    double startY = heat->elliptic->sq.leftBottom.y;
    double sizeLen = heat->elliptic->sq.sideLength;
    if (startX == NAN || startY == NAN || sizeLen == NAN) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    vector* X = linspace(startX, startX + sizeLen, heat->elliptic->n, allocator, err);
    if (X == NULL) {
        deleteVector(force, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* Y = linspace(startY, startY + sizeLen, heat->elliptic->n, allocator, err);
    if (Y == NULL) {
        deleteVector(force, err);
        deleteVector(X, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    double diameter = heat->elliptic->h;
    double a = heat->diffusivity;
    size_t row = 0;
    size_t col = 0;
    size_t counter = 0;
    double x = 0;
    double y = 0;
    double xLeft = X->data[0];
    double xRight = X->data[dim + 1];
    double yDown = Y->data[0];
    double yUp = Y->data[dim + 1];

    // Left down
    counter = 0;
    x = X->data[1];
    y = Y->data[1];
    force->data[counter] =
            omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(xLeft, y, t) +
            a * a * heat->tau / (diameter * diameter) * omp_ut(x, yDown, t) + previousLayer->data[counter];

    // Left up
    counter = dim - 1;
    x = X->data[1];
    y = Y->data[dim];
    force->data[counter] =
            omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(xLeft, y, t) +
            a * a * heat->tau / (diameter * diameter) * omp_ut(x, yUp, t) + previousLayer->data[counter];

    // Right down
    counter = (dim - 1) * dim;
    x = X->data[dim];
    y = Y->data[1];
    force->data[counter] =
            omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(xRight, y, t) +
            a * a * heat->tau / (diameter * diameter) * omp_ut(x, yDown, t) + previousLayer->data[counter];

    // Right up
    counter = dim * dim - 1;
    x = X->data[dim];
    y = Y->data[dim];
    force->data[counter] =
            omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(xRight, y, t) +
            a * a * heat->tau / (diameter * diameter) * omp_ut(x, yUp, t) + previousLayer->data[counter];

    // Left
#pragma omp parallel for shared(heat, force, previousLayer, t, X, Y, dim, a, xLeft, diameter, err) private(counter, row, x, y) default(none)
    for (row = 1; row < dim - 1; ++row) {
        counter = row;
        x = X->data[1];
        y = Y->data[row + 1];
        force->data[counter] =
                omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(xLeft, y, t) +
                previousLayer->data[counter];
    }


    // Down
#pragma omp parallel for shared(heat, force, previousLayer, t, X, Y, dim, a, yDown, diameter, err) private(counter, col, x, y) default(none)
    for (col = 1; col < dim - 1; ++col) {
        counter = col * dim;
        x = X->data[col + 1];
        y = Y->data[1];
        force->data[counter] =
                omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(x, yDown, t) +
                previousLayer->data[counter];
    }

    // Up
#pragma omp parallel for shared(heat, force, previousLayer, t, X, Y, dim, a, yUp, diameter, err) private(counter, col, x, y) default(none)
    for (col = 1; col < dim - 1; ++col) {
        counter = (col + 1) * dim - 1;
        x = X->data[col + 1];
        y = Y->data[dim];
        force->data[counter] =
                omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(x, yUp, t) +
                previousLayer->data[counter];
    }


    // Right
#pragma omp parallel for shared(heat, force, previousLayer, t, X, Y, dim, a, xRight, diameter, err) private(counter, row, x, y) default(none)
    for (row = 1; row < dim - 1; ++row) {
        counter = (dim - 1) * dim + row;
        x = X->data[dim];
        y = Y->data[row + 1];
        force->data[counter] =
                omp_f(x, y, t, a) * heat->tau + a * a * heat->tau / (diameter * diameter) * omp_ut(xRight, y, t) +
                previousLayer->data[counter];
    }


#pragma omp parallel for shared(heat, force, previousLayer, t, X, Y, dim, a, diameter, err) private(counter, row, col, x, y) collapse(2) default(none)
    for (col = 1; col < dim - 1; ++col) {
        for (row = 1; row < dim - 1; ++row) {
            counter = col * dim + row;
            x = X->data[col + 1];
            y = Y->data[row + 1];
            force->data[counter] = omp_f(x, y, t, a) * heat->tau + previousLayer->data[counter];
        }
    }
    return force;
}


vector* omp_fiveDiagMatrixMultiplyVec(heatTransfer* heat, sparseMatrix* A, vector* x,
                                      void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (heat == NULL || heat->elliptic == NULL || A == NULL || x == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = heat->elliptic->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    if (A->vec->n != numNoZero || x->n != dim * dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* vec = creatVector(dim * dim, allocator, err);
    if (vec == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    size_t counterSp;
    size_t counter;
    size_t row = 0;
    size_t col = 0;

    //Left down
    counter = 0;
    counterSp = 0;
    vec->data[counter] = A->vec->data[counterSp] * x->data[counter] +
                         A->vec->data[counterSp + 1] * x->data[counter + 1] +
                         A->vec->data[counterSp + 2] * x->data[counter + dim];

    // Left up
    counter = dim - 1;
    counterSp = 3 + (dim - 2) * 4;
    vec->data[counter] = A->vec->data[counterSp] * x->data[counter - 1] +
                         A->vec->data[counterSp + 1] * x->data[counter] +
                         A->vec->data[counterSp + 2] * x->data[counter + dim];

    // Right down
    counter = (dim - 1) * dim;
    counterSp = 2 * 3 + 3 * (dim - 2) * 4 + (dim - 2) * (dim - 2) * 5;
    vec->data[counter] = A->vec->data[counterSp] * x->data[counter - dim] +
                         A->vec->data[counterSp + 1] * x->data[counter] +
                         A->vec->data[counterSp + 2] * x->data[counter + 1];

    // Right up
    counter = dim * dim - 1;
    counterSp = 3 * 3 + 4 * (dim - 2) * 4 + (dim - 2) * (dim - 2) * 5;
    vec->data[counter] = A->vec->data[counterSp] * x->data[counter - dim] +
                         A->vec->data[counterSp + 1] * x->data[counter - 1] +
                         A->vec->data[counterSp + 2] * x->data[counter];

    // Left
#pragma omp parallel for shared(heat, A, x, vec, dim, err) private(counterSp, counter, row) default(none)
    for (row = 1; row < dim - 1; ++row) {
        counter = row;
        counterSp = 3 + (row - 1) * 4;
        vec->data[counter] = A->vec->data[counterSp] * x->data[counter - 1] +
                             A->vec->data[counterSp + 1] * x->data[counter] +
                             A->vec->data[counterSp + 2] * x->data[counter + 1] +
                             A->vec->data[counterSp + 3] * x->data[counter + dim];
    }

    // Down
#pragma omp parallel for shared(heat, A, x, vec, dim, err) private(counterSp, counter, col) default(none)
    for (col = 1; col < dim - 1; ++col) {
        counter = col * dim;
        counterSp = 2 * 3 + (dim - 2) * 4 + (col - 1) * (2 * 4 + (dim - 2) * 5);
        vec->data[counter] = A->vec->data[counterSp] * x->data[counter - dim] +
                             A->vec->data[counterSp + 1] * x->data[counter] +
                             A->vec->data[counterSp + 2] * x->data[counter + 1] +
                             A->vec->data[counterSp + 3] * x->data[counter + dim];
    }



    // Up
#pragma omp parallel shared(heat, A, x, vec, dim, err) private(counterSp, counter, col) default(none)
    for (col = 1; col < dim - 1; ++col) {
        counter = (col + 1) * dim - 1;
        counterSp = 2 * 3 + (dim - 2) * 4 + 4 + (dim - 2) * 5 + (col - 1) * (2 * 4 + (dim - 2) * 5);
        vec->data[counter] = A->vec->data[counterSp] * x->data[counter - dim] +
                             A->vec->data[counterSp + 1] * x->data[counter - 1] +
                             A->vec->data[counterSp + 2] * x->data[counter] +
                             A->vec->data[counterSp + 3] * x->data[counter + dim];
    }

    // Right
#pragma omp parallel shared(heat, A, x, vec, dim, err) private(counterSp, counter, row) default(none)
    for (row = 1; row < dim - 1; ++row) {
        counter = (dim - 1) * dim + row;
        counterSp = 3 * 3 + 3 * (dim - 2) * 4 + (dim - 2) * (dim - 2) * 5 + (row - 1) * 4;
        vec->data[counter] = A->vec->data[counterSp] * x->data[counter - dim] +
                             A->vec->data[counterSp + 1] * x->data[counter - 1] +
                             A->vec->data[counterSp + 2] * x->data[counter] +
                             A->vec->data[counterSp + 3] * x->data[counter + 1];
    }

    // inner
#pragma omp parallel for shared(heat, A, x, vec, dim, err) private(counterSp, counter, row, col) collapse(2) default(none)
    for (col = 1; col < dim - 1; ++col) {
        for (row = 1; row < dim - 1; ++row) {
            counter = col * dim + row;
            counterSp = 2 * 3 + (dim - 2) * 4 + (2 * 4 + (dim - 2) * 5) * (col - 1) + 4 + (row - 1) * 5;
            vec->data[counter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                 A->vec->data[counterSp + 1] * x->data[counter - 1] +
                                 A->vec->data[counterSp + 2] * x->data[counter] +
                                 A->vec->data[counterSp + 3] * x->data[counter + 1] +
                                 A->vec->data[counterSp + 4] * x->data[counter + dim];
        }
    }
    return vec;
}


vector*
omp_conjGrad(heatTransfer* heat, sparseMatrix* A, vector* b, void* const (* allocator)(size_t, size_t, myError*),
             myError* err) {
    if (heat == NULL || heat->elliptic == NULL || A == NULL || b == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = heat->elliptic->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    if (A->vec->n != numNoZero || b->n != dim * dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    vector* x = creatVector(dim * dim, allocator, err);
    if (x == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    vector* r1 = creatVector(dim * dim, allocator, err);
    if (r1 == NULL) {
        deleteVector(x, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    copyData(b, r1, err);

    vector* z = creatVector(dim * dim, allocator, err);
    if (z == NULL) {
        deleteVector(x, err);
        deleteVector(r1, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    copyData(r1, z, err);


    int flag = 1;
    vector* temp1;
    vector* r2;
    double tol = pow(10, -12);
    double alpha = 0;
    double beta = 0;
    while (flag) {

        temp1 = omp_fiveDiagMatrixMultiplyVec(heat, A, z, allocator, err);
        alpha = omp_dotProduct(r1, r1, err) / omp_dotProduct(temp1, z, err);
        deleteVector(temp1, err);

        // x = alpha * z + x;
        temp1 = omp_saxpy(alpha, z, x, allocator, err);
        copyData(temp1, x, err);
        deleteVector(temp1, err);

        // r2 = -alpha * A * z + r1;
        temp1 = omp_fiveDiagMatrixMultiplyVec(heat, A, z, allocator, err);
        r2 = omp_saxpy(-alpha, temp1, r1, allocator, err);
        deleteVector(temp1, err);

        beta = omp_dotProduct(r2, r2, err) / omp_dotProduct(r1, r1, err);

        // z = beta * z + r2;
        temp1 = z;
        z = omp_saxpy(beta, z, r2, allocator, err);
        deleteVector(temp1, err);

        flag = (omp_norm2(r2, err) / omp_norm2(b, err)) > tol;
        deleteVector(r1, err);
        r1 = r2;
    }
    deleteVector(z, err);
    deleteVector(r1, err);
    return x;
}


vector* omp_findEndTime(heatTransfer* heat, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (heat == NULL || heat->elliptic == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = heat->elliptic->n - 2;
    sparseMatrix* const stiff = omp_calStiffness(heat, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    if (stiff == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* previousLayer = creatVector(dim * dim, allocator, err);
    if (previousLayer == NULL) {
        deleteSparseMatrix(stiff, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    double startX = heat->elliptic->sq.leftBottom.x;
    double startY = heat->elliptic->sq.leftBottom.y;
    double sizeLen = heat->elliptic->sq.sideLength;
    if (startX == NAN || startY == NAN || sizeLen == NAN) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* X = linspace(startX, startX + sizeLen, heat->elliptic->n, allocator, err);
    if (X == NULL) {
        deleteSparseMatrix(stiff, err);
        deleteVector(previousLayer, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* Y = linspace(startY, startY + sizeLen, heat->elliptic->n, allocator, err);
    if (Y == NULL) {
        deleteSparseMatrix(stiff, err);
        deleteVector(previousLayer, err);
        deleteVector(X, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t counter = 0;
    size_t col = 0;
    size_t row = 0;
    double t = 0;

#pragma omp parallel for shared(previousLayer, t, X, Y, dim, err) private(counter, row, col) collapse(2) default(none)
    for (col = 0; col < dim; ++col) {
        for (row = 0; row < dim; ++row) {
            counter = col * dim + row;
            double x = X->data[col + 1];
            double y = Y->data[row + 1];
            previousLayer->data[counter] = omp_ut(x, y, t);
        }
    }

    deleteVector(X, err);
    deleteVector(Y, err);
    vector* currentLayer;
    double tol = pow(10, -12);
    int flag = 1;
    size_t n = 0;
    while (flag) {
        t += heat->tau;
        vector* force = omp_calForce(heat, previousLayer, t, allocator, err);
        if (force == NULL) {
            deleteSparseMatrix(stiff, err);
            deleteVector(previousLayer, err);
            reportError(*err, __FILE__, __FUNCTION__, __LINE__);
            return NULL;
        }
        currentLayer = omp_conjGrad(heat, stiff, force, allocator, err);
        vector* diff = omp_saxpy(-1, previousLayer, currentLayer, allocator, err);
        double temp = omp_normInf(diff, err);
        //printf("temp = %g\n", temp);
        flag =  n < 10; // temp > tol;
        copyData(currentLayer, previousLayer, err);
        deleteVector(force, err);
        deleteVector(currentLayer, err);
        deleteVector(diff, err);
        n++;
    }
    deleteSparseMatrix(stiff, err);
    heat->endTime = t;
    return previousLayer;
}


double omp_calErrInf(heatTransfer* heat, vector* v, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (heat == NULL || heat->elliptic == NULL || v == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t dim = heat->elliptic->n - 2;
    double startX = heat->elliptic->sq.leftBottom.x;
    double startY = heat->elliptic->sq.leftBottom.y;
    double sizeLen = heat->elliptic->sq.sideLength;
    vector* X = linspace(startX, startX + sizeLen, heat->elliptic->n, allocator, err);
    if (X == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    vector* Y = linspace(startY, startY + sizeLen, heat->elliptic->n, allocator, err);
    if (Y == NULL) {
        deleteVector(X, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t counter = 0;
    size_t col = 0;
    size_t row = 0;
    double sum = 0;

#pragma omp parallel for shared(v, sum, X, Y, dim,  err) private(counter, row, col) collapse(2) default(none)
    for (col = 0; col < dim; ++col) {
        for (row = 0; row < dim; ++row) {
            counter = col * dim + row;
            double x = X->data[col + 1];
            double y = Y->data[row + 1];
            if (sum < fabs(v->data[counter] - omp_u(x, y)))
                sum = fabs(v->data[counter] - omp_u(x, y));
            counter++;
        }
    }
    deleteVector(X, err);
    deleteVector(Y, err);
    return sum;
}

void deleteEllipticProb(ellipticProb* ell, myError* err) {
    if (ell == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    //free(ell->indexLeft);
    free(ell);
}


void deleteHeat(heatTransfer* heat, myError* err) {
    if (heat == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    deleteEllipticProb(heat->elliptic, err);
    free(heat);
}