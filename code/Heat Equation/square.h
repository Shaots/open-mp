//
// Created by Lenovo on 21.10.2023.
//

#ifndef PARALAB_SQUARE_H
#define PARALAB_SQUARE_H

#include <stdio.h>
#include <stdlib.h>


typedef struct point {
    double x;
    double y;
} point;


typedef struct square {
    point leftBottom;
    double sideLength;
} square;


square creatSquare(point leftBottom, double sideLength);


void paintSquare(square sq);


#endif //PARALAB_SQUARE_H
