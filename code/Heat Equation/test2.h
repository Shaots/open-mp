//
// Created by Lenovo on 01.12.2023.
//

#ifndef PARAMAIN_TEST2_H
#define PARAMAIN_TEST2_H

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "heatEquation.h"
#include "omp_vector.h"


void testVector(myError* err);


void testHeat(myError* err);


#endif //PARAMAIN_TEST2_H
