//
// Created by Lenovo on 21.11.2023.
//
#include <stdlib.h>
#include <stdio.h>
#include "test2.h"


int main() {
#ifdef _OPENMP
    printf("OpenMP is supported!\n");
#endif

    myError* err;
    FILE* errorFile;
    err = calloc(1, sizeof(myError));
    errorFile = fopen("err.txt", "ab");
    setLogger(errorFile);

    testHeat(err);
    free(err);
    fclose(errorFile);
    return 0;
}
