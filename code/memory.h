//
// Created by Lenovo on 21.10.2023.
//

#ifndef OPEN_MP_MEMORY_H
#define OPEN_MP_MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include "error.h"


extern size_t memorySize;


void* allocMemory(size_t numOfElement, size_t sizeOfElement, myError* err);


#endif //OPEN_MP_MEMORY_H
