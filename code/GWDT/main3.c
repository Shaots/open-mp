//
// Created by Lenovo on 21.11.2023.
//

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "pgm.h"
#include "test.h"


int main(int argc, char** argv) {
#ifdef _OPENMP
    printf("OpenMP is supported!\n");
#endif

    myError* err;
    FILE* errorFile;
    err = calloc(1, sizeof(myError));
    errorFile = fopen("err.txt", "ab");
    setLogger(errorFile);

    const char* filename = "chess.pgm";
    int width = 1920;
    int height = 1080;
    creatChess(filename, width, height);
    test(filename, err);

    free(err);
    fclose(errorFile);
    return 0;
}
