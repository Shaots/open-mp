//
// Created by PC on 06.01.2024.
//

#ifndef PARAMAIN_GWDT_H
#define PARAMAIN_GWDT_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "pgm.h"
#include "mmath.h"
#include "../memory.h"
#include "../error.h"

#define TRUE 1
#define FALSE (-1)


typedef struct GWDT {
    int width;
    int height;
    double* data;
} GWDT;


GWDT* createGWDT(int width, int height, void* const (* allocator)(size_t, size_t, myError*), myError* err);


GWDT* initGWDT(GWDT* myGWDT, myError* err);


void copyGWDT(GWDT* from, GWDT* to, myError* err);


GWDT* eikonal(pgm* image, GWDT* previousLayer, GWDT* nextLayer, myError* err);


int compare2Layer(GWDT* previousLayer, GWDT* nextLayer, myError* err);


pgm* scale(const GWDT* myGWDT, myError* err);


void deleteGWDT(GWDT* myGWDT, myError* err);


void paintGWDT(GWDT* myGWDT, myError* err);

#endif //PARAMAIN_GWDT_H
