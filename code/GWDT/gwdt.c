//
// Created by PC on 06.01.2024.
//

#include "gwdt.h"

GWDT* createGWDT(int width, int height, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    GWDT* myGWDT = (GWDT*) allocator(1, sizeof(GWDT), err);
    if (myGWDT == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    myGWDT->height = height;
    myGWDT->width = width;
    myGWDT->data = (double*) allocator(height * width, sizeof(double), err);
    if (myGWDT->data == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        free(myGWDT);
        return NULL;
    }
    return myGWDT;
}


GWDT* initGWDT(GWDT* myGWDT, myError* err) {
    if (myGWDT == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int height = myGWDT->height;
    int width = myGWDT->width;
    const int source1[2] = {0, 0};
    const int source2[2] = {height - 1, width - 1};
    int i, j;
#pragma omp parallel for shared(myGWDT, height, width, source1, source2) private(i, j) collapse(2) default(none)
    for (i = 0; i < height; ++i) {
        for (j = 0; j < width; ++j) {
            if ((i == source1[0] && j == source1[1]) || (i == source2[0] && j == source2[1]))
                myGWDT->data[i * width + j] = 0;
            else
                myGWDT->data[i * width + j] = INFINITY;
        }
    }
    return myGWDT;
}


void copyGWDT(GWDT* from, GWDT* to, myError* err) {
    if (from == NULL || to == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    if (from->width != to->width || from->height != to->height) {
        *err = ERROR_DIMENSION;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    memcpy(to->data, from->data, from->width * from->height * sizeof(double));
}


GWDT* eikonal(pgm* image, GWDT* previousLayer, GWDT* nextLayer, myError* err) {
    if (image == NULL || previousLayer == NULL || nextLayer == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    if (image->data == NULL || previousLayer->data == NULL || nextLayer->data == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    if (image->width != previousLayer->width || previousLayer->width != nextLayer->width) {
        *err = ERROR_DIMENSION;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    if (image->height != previousLayer->height || previousLayer->height != nextLayer->height) {
        *err = ERROR_DIMENSION;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }


    double a = 1;
    double b = sqrt(2);
    double c = sqrt(5);
    double d = 2;
    double e = 2 * sqrt(2);
    int width = image->width;
    int height = image->height;

    int i, j;
#pragma omp parallel for shared(nextLayer, previousLayer, image, height, width, a, b, c, d, e, err) private(i, j) default(none)
    for (i = 2; i <= height - 1; ++i) {
        for (j = 2; j <= width - 3; ++j) {
            const double arrUp[13] = {nextLayer->data[(i - 1) * width + j] + a * (double) image->data[i * width + j],
                                      nextLayer->data[i * width + (j - 1)] + a * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 1) * width + (j - 1)] +
                                      b * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 1) * width + (j + 1)] +
                                      b * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 1) * width + (j + 2)] +
                                      c * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 1) * width + (j - 2)] +
                                      c * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 2) * width + (j - 1)] +
                                      c * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 2) * width + (j + 1)] +
                                      c * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 2) * width + j] + d * (double) image->data[i * width + j],
                                      nextLayer->data[i * width + (j - 2)] + d * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 2) * width + (j - 2)] +
                                      e * (double) image->data[i * width + j],
                                      nextLayer->data[(i - 2) * width + (j + 2)] +
                                      e * (double) image->data[i * width + j],
                                      previousLayer->data[i * width + j]};
            nextLayer->data[i * width + j] = findMin(arrUp, 13, err);
        }
    }

#pragma omp parallel for shared(nextLayer, previousLayer, image, height, width, a, b, c, d, e, err) private(i, j) default(none)
    for (i = height - 3; i >= 0; --i) {
        for (j = width - 3; j >= 2; --j) {
            const double arrDown[13] = {nextLayer->data[(i + 1) * width + j] + a * (double) image->data[i * width + j],
                                        nextLayer->data[i * width + (j + 1)] + a * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 1) * width + (j + 1)] +
                                        b * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 1) * width + (j - 1)] +
                                        b * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 1) * width + (j - 2)] +
                                        c * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 1) * width + (j + 2)] +
                                        c * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 2) * width + (j + 1)] +
                                        c * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 2) * width + (j - 1)] +
                                        c * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 2) * width + j] + d * (double) image->data[i * width + j],
                                        nextLayer->data[i * width + (j + 2)] + d * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 2) * width + (j + 2)] +
                                        e * (double) image->data[i * width + j],
                                        nextLayer->data[(i + 2) * width + (j - 2)] +
                                        e * (double) image->data[i * width + j],
                                        previousLayer->data[i * width + j]};
            nextLayer->data[i * width + j] = findMin(arrDown, 13, err);
        }
    }
    return nextLayer;
}


int compare2Layer(GWDT* previousLayer, GWDT* nextLayer, myError* err) {
    if (previousLayer == NULL || nextLayer == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return FALSE;
    }
    if (previousLayer->data == NULL || nextLayer->data == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return FALSE;
    }
    if (previousLayer->width != nextLayer->width || previousLayer->height != nextLayer->height) {
        *err = ERROR_DIMENSION;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return FALSE;
    }
    int width = previousLayer->width;
    int height = previousLayer->height;

    int flag = 0;
    int i, j;
#pragma omp parallel for shared(previousLayer, nextLayer, height, width) private(i, j) collapse(2) reduction(+: flag) default(none)
    for (i = 0; i < height; ++i) {
        for (j = 0; j < width; ++j) {
            if (previousLayer->data[i * width + j] != nextLayer->data[i * width + j])
                flag++;
        }
    }
    return flag;
}


pgm* scale(const GWDT* myGWDT, myError* err) {
    if (myGWDT == NULL || myGWDT->data == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int newWidth = myGWDT->width - 4;
    int newHeight = myGWDT->height - 4;
    int newMax = 0;
    pgm* image = createPGM(newWidth, newHeight, newMax, err);
    if (image == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int width = myGWDT->width;
    int height = myGWDT->height;
    double min = myGWDT->data[2 * width + 2];
    double max = myGWDT->data[2 * width + 2];
    for (int i = 2; i < height - 2; i++) {
        for (int j = 2; j < width - 2; j++) {
            if (myGWDT->data[i * width + j] < min) {
                min = myGWDT->data[i * width + j];
            }
            if (myGWDT->data[i * width + j] > max) {
                max = myGWDT->data[i * width + j];
            }
        }
    }

    double factor = max - min != 0 ? 255 / (max - min) : 255 / max;
    for (int i = 2; i < height - 2; i++) {
        for (int j = 2; j < width - 2; j++) {
            int x = i - 2;
            int y = j - 2;
            image->data[x * newWidth + y] = (int) ((myGWDT->data[i * width + j] - min) * factor);
        }
    }
    return image;
}


void deleteGWDT(GWDT* myGWDT, myError* err) {
    if (myGWDT == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    free(myGWDT->data);
    free(myGWDT);
}


void paintGWDT(GWDT* myGWDT, myError* err) {
    if (myGWDT == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    for (int i = 0; i < myGWDT->height; ++i) {
        for (int j = 0; j < myGWDT->width; ++j) {
            printf("%.2lf ", myGWDT->data[i * myGWDT->width + j]);
        }
        printf("\n");
    }
}