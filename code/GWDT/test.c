//
// Created by PC on 06.01.2024.
//

#include "test.h"
#include "gwdt.h"
#include "pgm.h"

void test(const char* filename, myError* err) {
    pgm* myPGM = readPGM(filename, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    if (myPGM == NULL) {
        *err = ERROR_FILE_CANNOT_BE_OPENED;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    GWDT* nextLayer;
    GWDT* previousLayer;
    int times = 1;
    double start_time, end_time;
    omp_set_num_threads(1);
    start_time = omp_get_wtime();
    for (int i = 0; i < times; ++i) {
        nextLayer = createGWDT(myPGM->width, myPGM->height,
                               (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        nextLayer = initGWDT(nextLayer, err);
        previousLayer = createGWDT(myPGM->width, myPGM->height,
                                   (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        copyGWDT(nextLayer, previousLayer, err);
        size_t n = 0;
        int flag = 1;
        while (flag) {
            nextLayer = eikonal(myPGM, previousLayer, nextLayer, err);
            flag = compare2Layer(previousLayer, nextLayer, err);
            printf("%d\n", flag);
            copyGWDT(nextLayer, previousLayer, err);
            n++;
        }
        pgm* out = scale(nextLayer, err);
        //paintPGM(out, err);
        writePGM(out, "out.pgm", err);
        deleteGWDT(nextLayer, err);
        deleteGWDT(previousLayer, err);
    }
    end_time = omp_get_wtime();
    /*FILE* file = fopen("T(N).txt", "ab");
    fprintf(file, "%lf\n", (end_time - start_time) / (double) times);*/
    //pgm* out = scale(nextLayer, err);
    //paintPGM(out, err);
    //writePGM(out, "out.pgm", err);
    //deletePGM(out, err);
    deletePGM(myPGM, err);
}