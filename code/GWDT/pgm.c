//
// Created by PC on 05.01.2024.
//


#include "pgm.h"


pgm* createPGM(int width, int height, int max, myError* err) {
    pgm* myPGM = (pgm*) allocMemory(1, sizeof(pgm), err);
    if (myPGM == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    myPGM->height = height;
    myPGM->width = width;
    myPGM->max = max;
    myPGM->data = (int*) allocMemory(height * width, sizeof(int), err);
    if (myPGM->data == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        free(myPGM);
        return NULL;
    }
    return myPGM;
}


pgm* readPGM(const char* filename, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    pgm* myPGM = NULL;
    FILE* file;
    char str[256];
    file = fopen(filename, "r");
    if (file == NULL) {
        *err = ERROR_FILE_CANNOT_BE_OPENED;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    fscanf(file, "%s", str);

    int width;
    fscanf(file, "%d", &width);

    int height;
    fscanf(file, "%d", &height);

    int maxGray;
    fscanf(file, "%d", &maxGray);

    myPGM = createPGM(width, height, maxGray, err);

    unsigned char* line = (unsigned char*) allocator(width, sizeof(unsigned char), err);
    for (int i = 0; i < height; ++i) {
        fread(line, sizeof(unsigned char), width, file);
        for (int j = 0; j < width; ++j) {
            myPGM->data[i * width + j] = line[j];
        }
    }
    fclose(file);
    return myPGM;
}


void writePGM(const pgm* image, const char* filename, myError* err) {
    if (image == NULL || image->data == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    FILE* file = fopen(filename, "wb");
    int width = image->width;
    int height = image->height;
    fprintf(file, "P5\n");
    fprintf(file, "%d %d\n", width, height);
    fprintf(file, "%d\n", image->max != 0 ? image->max : 255);
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            fputc((image->data[i * width + j]) & 0x000000FF, file);
        }
    }
    fclose(file);
}


void creatChess(const char* filename, int width, int height) {
    FILE* file = fopen(filename, "wb");
    int sizeBlock = 60;
    fprintf(file, "P5\n");
    fprintf(file, "%d %d\n", width, height);
    fprintf(file, "%d\n", 255);
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            int r1 = i / sizeBlock;
            int r2 = j / sizeBlock;
            if (r1 % 2 == r2 % 2)
                fputc(255 & 0x000000FF, file);
            else
                fputc(0 & 0x000000FF, file);
        }
    }
    fclose(file);
}


void deletePGM(pgm* myPGM, myError* err) {
    if (myPGM == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    free(myPGM->data);
    free(myPGM);
}


void paintPGM(pgm* myPGM, myError* err) {
    if (myPGM == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    for (int i = 0; i < myPGM->height; ++i) {
        for (int j = 0; j < myPGM->width; ++j) {
            printf("%d ", myPGM->data[i * myPGM->width + j]);
        }
        printf("\n");
    }
}