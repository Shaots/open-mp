//
// Created by Lenovo on 06.01.2024.
//

#ifndef OPEN_MP_MMATH_H
#define OPEN_MP_MMATH_H


#include <stdlib.h>
#include <stdlib.h>
#include <math.h>
#include "../error.h"


double findMin(const double* arr, size_t len, myError* err);



#endif //OPEN_MP_MMATH_H
