//
// Created by PC on 05.01.2024.
//

#ifndef PARAMAIN_PGM_H
#define PARAMAIN_PGM_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../error.h"
#include "../memory.h"


typedef struct pgm {
    int width;
    int height;
    int max;
    int* data;
} pgm;


pgm* createPGM(int width, int height, int max, myError* err);


pgm* readPGM(const char* filename, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void writePGM(const pgm* image, const char* filename, myError* err);


void creatChess(const char* filename, int width, int height);


void deletePGM(pgm* myPGM, myError* err);


void paintPGM(pgm* myPGM, myError* err);

#endif //PARAMAIN_PGM_H
