//
// Created by PC on 06.01.2024.
//

#ifndef PARAMAIN_TEST_H
#define PARAMAIN_TEST_H

#include <omp.h>
#include "../error.h"



void test(const char* filename, myError* err);


#endif //PARAMAIN_TEST_H
